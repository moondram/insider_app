'use strict';

crop.factory('cropArea', ['cropCanvas', function(CropCanvas) {
  var CropArea = function(ctx, events) {
    this._ctx=ctx;
    this._events=events;

    this._minSize=80;

    this._cropCanvas=new CropCanvas(ctx);

    this._image=new Image();
    this._x = 0;
    this._y = 0;
    this._size = 200;
    this._size_x = 0;
    this._size_y = 0;
       
  };

  /* GETTERS/SETTERS */

  CropArea.prototype.getImage = function () {
    return this._image;
  };
     
  CropArea.prototype.setImage = function (image) {
    this._image = image;
  };

  CropArea.prototype.getX = function () {
     console.log("getting X");         
    return this._x;
  };
     
  CropArea.prototype.setX = function (x) {
    this._x = x;
    
    this._dontDragOutside();
  };

  CropArea.prototype.setCropX = function (y) {
    this._size_x = y;
    console.log("set X");
    this._dontDragOutside();
  };
     
  CropArea.prototype.getCropX = function () {
    return this._size_x;
  };
  
  CropArea.prototype.getCropRatio = function (axis) {
    var maxAxis = Math.max(this._size_y,this._size_x);
    var minAxis = Math.min(this._size_y,this._size_x);
    var cropRatio = 1;
       
    if((this._size_y > this._size_x) && (axis == "X"))
    {
        cropRatio = this._size_x/this._size_y;        
    }
    else if((this._size_y < this._size_x) && (axis == "Y"))
    {
        cropRatio = this._size_y/this._size_x;          
    }
       
    return cropRatio;
         
  };
 
     
  CropArea.prototype.getY = function () {
    return this._y;
  };
     
  CropArea.prototype.setY = function (y) {
    this._y = y;
    console.log("setting Y");   
    this._dontDragOutside();
  };
     
  CropArea.prototype.setCropY = function (y) {
    this._size_y = y;
    console.log("set Y");
    this._dontDragOutside();
  };

  CropArea.prototype.getCropY = function () {
    return this._size_y;
  };
  
//   CropArea.prototype.getCropY = function () {
//     return this._size_y;
//   };   
     
  CropArea.prototype.getSize = function () {
    return this._size;
  };
  CropArea.prototype.setSize = function (size) {
    this._size = Math.max(this._minSize, size);
    this._dontDragOutside();
  };

  CropArea.prototype.getMinSize = function () {
    return this._minSize;
  };
  CropArea.prototype.setMinSize = function (size) {
    this._minSize = size;
    this._size = Math.max(this._minSize, this._size);
    this._dontDragOutside();
  };

  /* FUNCTIONS */
  CropArea.prototype._dontDragOutside=function() {
    var h=this._ctx.canvas.height,
        w=this._ctx.canvas.width;
//     if(this._x<this._size/2 * this.getCropRatio("X") ) { this._x=this._size/2 * this.getCropRatio("X"); }
//     if(this._x>w-this._size/2 * this.getCropRatio("X") ) { this._x=w-this._size/2 * this.getCropRatio("X"); }
//     if(this._y<this._size/2 * this.getCropRatio("Y") ) { this._y=this._size/2 * this.getCropRatio("Y"); }
//     if(this._y>h-this._size/2 * this.getCropRatio("Y") ) { this._y=h-this._size/2 * this.getCropRatio("Y"); }
       if(this._size>w) { this._size=w; }
       if(this._size>h) { this._size=h; }
       if(this._x<this._size/2) { this._x=this._size/2; }
       if(this._x>w-this._size/2) { this._x=w-this._size/2; }
       if(this._y<this._size/2) { this._y=this._size/2; }
       if(this._y>h-this._size/2) { this._y=h-this._size/2; }
  };

  CropArea.prototype._drawArea=function() {};

  CropArea.prototype.draw=function() {
    // draw crop area
    this._cropCanvas.drawCropArea(this._image,[this._x,this._y],this._size,this._drawArea);
  };

  CropArea.prototype.processMouseMove=function() {};

  CropArea.prototype.processMouseDown=function() {};

  CropArea.prototype.processMouseUp=function() {};

  return CropArea;
}]);