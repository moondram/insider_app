'use strict';


// Declare app level module which depends on filters, and services
var inApp = angular.module('inApp', ['ngRoute','ngImgCrop','angularFileUpload']);

inApp.controller("myCtrl", ['$scope','$rootScope','$upload','$http',function($scope,$rootScope,$upload,$http) {
    $scope.firstName = "John";
    $scope.lastName = "Doe";
    $scope.myImage='';
    $scope.myCroppedImage='';
    $scope.myCroppedImage1 ='';
    $scope.myCroppedImage2 ='';
    $scope.myCroppedImage3 =''; 
    $scope.previewImage = "show";
    $scope.cloudImage = "hidden";
     
     
    $rootScope.imageArray = [];
    $rootScope.AreaSize = "";
    $rootScope.AreaX = ""; 
    $rootScope.AreaY = ""; 
    $scope.image_url_array = [];
    $scope.View_image_array = [];
     
    $scope.image_uploaded_flag = false; 
    $scope.image_upload_status = "Not uploaded";  
     
    var handleFileSelect=function(evt) {
          var file=evt.currentTarget.files[0];
          
         
          var reader = new FileReader();
          reader.onload = function (evt) {
            $scope.$apply(function($scope){
                               
             
              $scope.myImage=evt.target.result;
              $scope.previewImage = "show";
              $scope.image_upload_status = "Not uploaded";
              
              
            });
          console.log("got it");     
          };
          reader.readAsDataURL(file);
         
        };
     
   angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);  
     
   $scope.updateResult = function(index) {
        console.log("updateing result");
        
   };   
     

     $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 1; i <= files.length; i++) {
                var file = files[i];
                console.log(file);
                $upload.upload({
                    url: '/api/user/uploads',
                    fields: {'username': "hello"},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                   // console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file  uploaded. Response: ' + data);
                });
            }
        }
      };
          
     $scope.upload_files = function(){
          $scope.upload($rootScope.imageArray);
          console.log($rootScope.imageArray);
     }
     
     $scope.upload_image = function() {
          $scope.upload_json($rootScope.imageArray);
     }
     
     $scope.upload_json = function(data) {
          $scope.image_upload_status = "Uploading files...";
          
         
          $scope.image_url_array = [];
          $scope.image_url_array.length = 0;
         
          
          var i = 0;
          for(i = 1 ; i < 5 ; i++)
          {
               (function(i){
                    $http({
                         method: 'POST',
                         url: '/api/user/uploads',
                         data: { 'jj' : data[i] }
                    })
                         .success(function(data) {
                         console.log(data);
                         var temp_url = data;
                         $scope.image_url_array[i] = temp_url.url;
                         console.log("done");
                    })
                         .error(function() {
                         console.log("not done");
                    });      
               })(i);
                        
          }

     }
     
     $scope.$watch(function() { return $scope.image_url_array.length },
              function(newValue, oldValue) {
                   console.log(newValue);
                   if(newValue  >=  4) {
                        $scope.image_uploaded_flag = true;
                        $scope.image_upload_status = "Uploaded"
                        console.log("All images uploaded");
                  }
     });
     

     
     
     
     
     
     
     
     
}]);
