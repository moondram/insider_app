
/**
 * Module dependencies
 */

var express = require('express'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
//  multiparty = require('connect-multiparty'),
//  multipartyMiddleware = multiparty(),
  path = require('path');
var cloudinary = require('cloudinary').v2;

var app = module.exports = express();

var UserController = require('./controllers/UserController');

/**
* Configuration
*/

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// development only
if (app.get('env') === 'development') {
   app.use(express.errorHandler());
};

// production only
if (app.get('env') === 'production') {
  // TODO
}; 



// Routes
//p.get('/', routes.index);
app.get('/', function(req, res){
  res.render('public/index.html');
});
app.get('/partial/:name', routes.partial);

// JSON API
app.get('/api/name', api.name);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

// Example endpoint 
//app.post('/api/user/uploads', multipartyMiddleware, UserController.uploadFile);
app.post('/api/user/uploads', UserController.upload_json);


/**
* Start Server
*/

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});